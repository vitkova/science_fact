desc "This task will send emails and will be run by the Heroku scheduler"
task :send_emails => :environment do
  puts "Sending emails"
  Child.send_scheduled_emails
end