desc "This task will create unsubscribe tokens for all existing users"
task :create_unsubscribe_tokens => :environment do
 Child.all.each do |child|
  if child.unsubscribe_token.nil?
    child.unsubscribe_token = SecureRandom.uuid
    child.save
  end
 end
end