desc "This task will update last_fact_date based on history table"
task :update_last_fact_date => :environment do
  count = 0
  updated = 0
  Child.all.each do |child|
    history = History.where(child_id: child.id).order(created_at: :desc).limit(1).first
    if history.created_at.strftime("%m/%d/%y") != child.last_fact_date.strftime("%m/%d/%y")
      child.last_fact_date = history.created_at
      child.save
      updated = updated + 1
    end
    count = count + 1
  end
  print updated.to_s + " of " + count.to_s + " records updated!" + "\n"
end