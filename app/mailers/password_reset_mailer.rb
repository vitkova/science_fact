class PasswordResetMailer < ApplicationMailer
  default from: "Curious Tots <noreply@curious-tots.com>"
  
  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Password reset"
  end
  
end
