class FactMailer < ApplicationMailer
  default from: "Curious Tots <noreply@curious-tots.com>"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.fact_mailer.fact_email.subject
  #
  def fact_email(user, child, fact)
    @greeting = "Hi " + child.name
    @fact = fact.content
    @source = ""
    if !fact.reference.nil?
      @source = "Source: " + fact.reference.citation
    end
    @child_id = child.id
    @unsubscribe_token = child.unsubscribe_token
    mail to: user.email, subject: "Today's science fact for " + child.name
  end
end
