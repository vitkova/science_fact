class WelcomeMailer < ApplicationMailer
  default from: "Curious Tots <noreply@curious-tots.com>"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.welcome_mailer.welcome_email.subject
  #
  def welcome_email(user)
    mail to: user.email, subject: "Welcome to Curious Tots!"
  end
end
