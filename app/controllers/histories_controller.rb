class HistoriesController < ApplicationController
  before_action :correct_parent_or_admin
  
  
  
  def index
    @child = Child.find(params[:child_id])
    @histories = History.child_history(@child).order(created_at: :desc)
  end
  
  private 
    
    def correct_parent_or_admin
        @child = current_user.children.find_by(id: params[:child_id])
        redirect_to root_url unless !@child.nil? || current_user.admin
    end
  
end
