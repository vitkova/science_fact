class SubscriptionsController < ApplicationController
  
  def edit
    child = Child.find_by(id: params[:id])
    if params[:active] == "1"
      subscribe_action(child)
    else
      unsubscribe_action(child)
    end
  end
  
  private
    
    def subscribe_action(child)
      @user = current_user
      child.subscribe
      redirect_to @user
    end
    
    def unsubscribe_action(child)
      if child && child.unsubscribe_token == params[:unsubscribe_token]
        child.unsubscribe
        flash.now[:success] = child.name + " has been unsubscribed."
        render 'subscriptions/edit'
      else
        flash[:danger] = "Invalid unsubscribe link."
        redirect_to root_url
      end
    end
  
end
