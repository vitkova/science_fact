class StatsController < ApplicationController
  before_action :logged_in_user
  before_action :admin_user
  
  def show
    @user_count = User.admin(false).count
    @child_count= Child.count
    @fact_count = Fact.active(true).count
    @history_count = History.count
  end
  
end
