class SessionsController < ApplicationController
  def new
  end
  
  def create
    
    user = User.find_by(email: params[:session][:email].downcase)
    
    if user && user.authenticate(params[:session][:password])
      # login
      log_in(user)
      redirect_to root_url    
    else
      flash.now[:danger] = 'User NOT found!'
      render 'new'
    end
  end

  def destroy
    logout(current_user)
    redirect_to root_url
  end

end

