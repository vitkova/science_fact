class ChildrenController < ApplicationController
  before_action :logged_in_user
  before_action :correct_parent, only: [:edit, :update]
  
  def new
    @child = Child.new
  end
  
  def create
    @user = current_user
    @child = @user.children.build(child_params)
    if @child.save
      @child.send_fact_email
      flash[:success] = @child.name + " has been added.  Check your email for " + @child.name.possessive + " first science fact!"
      redirect_to @user
    else
      render 'children/new'
    end
  end
  
  def edit
    @child = Child.find(params[:id])
  end
  
  def update
    @child = Child.find(params[:id])
    if @child.update_attributes(child_params)
      flash[:success] = @child.name.possessive + " info has been updated!"
      redirect_to @child.user
    else
      render 'edit'
    end
  end
  
  def show
    @children = current_user.children
  end
  
  private
  
    def child_params
      params.require(:child).permit(:name, :dob)
    end
    
#    def correct_parent
#      @child = current_user.children.find_by(id: params[:id])
#      redirect_to root_url if @child.nil?
#    end
  
end
