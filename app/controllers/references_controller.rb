class ReferencesController < ApplicationController
  before_action :logged_in_user
  before_action :admin_user
  
  def new
    @reference = Reference.new
  end
  
  def create
    @reference = Reference.new(reference_params)
    
    if @reference.save
       redirect_to references_url
       flash[:success] = "Reference has been created!"
    else
      render 'new'
    end
    
  end

  def edit
    @reference = Reference.find(params[:id])
  end
  
  def update
    # find the reference to be updated based on :id attribute
    @reference = Reference.find(params[:id])
    
    if @reference.update_attributes(reference_params)
      flash[:success] = "Reference updated!"
      redirect_to references_url
    else
      render 'edit'
    end
  end
  
  def index
    @references = Reference.all.order(:citation)
  end
  
  private
  
    def reference_params
      params.require(:reference).permit(:citation)
    end
  
end
