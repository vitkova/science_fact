class UsersController < ApplicationController
  before_action :logged_in_user, only: [:show]
  before_action :correct_user_or_admin, only: [:show]
  before_action :admin_user, only: [:index, :edit, :update]

  def new
    @user = User.new
    @new_admin = false
    @new_admin = true if params[:admin] == "true"
  end

  def create
    @user = User.new(user_params)
    if @user.save
      if @user.admin?
        redirect_to users_path(admin:true)
      else
        log_in(@user)
        WelcomeMailer.welcome_email(@user).deliver_now
        redirect_to new_child_url
      end
    else
      if @user.admin?
        @new_admin = true
      end
      render 'new'
    end
  end
  
  def edit
    @admin = User.find(params[:id])
  end
  
  def update
    @admin = User.find(params[:id])
    if @admin.update_attributes(user_params)
      flash[:success] = @admin.fname + " " + @admin.lname.possessive + " info has been updated!"
      redirect_to users_path(admin:true)
    else
      render 'edit'
    end
  end

  def show
    @user = User.find(params[:id])
    @children = @user.children.order(:name)
  end
  
  def index
    @show_admins = nil
    if params[:admin] == "true"
      @users = User.admin(true)
      @show_admins = true
    else
      @users = User.admin(false)
      @show_admins = false
    end
  end
  
  private

    def user_params
      params.require(:user).permit(:fname, :lname, :email, :password,
                                     :password_confirmation, :admin)
    end
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to root_url unless current_user?(@user)
    end
    
    def correct_user_or_admin
      @user = User.find(params[:id])
      redirect_to root_url unless admin_user? || current_user?(@user)
    end
  
end
