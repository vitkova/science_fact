class FactsController < ApplicationController
  before_action :logged_in_user
  before_action :admin_user
  
  def new
      @fact = Fact.new
      @citations = Reference.all
  end
  
  def create
    
    @fact = Fact.new(fact_params)
    
    if @fact.save
      redirect_to facts_url
    else
      render 'new'
    end
  end
  
  def edit
    @fact = Fact.find(params[:id])
  end
  
  def update
    
    # find the fact to be updated based on :id attribute
    @fact = Fact.find(params[:id])
    
    if @fact.update_attributes(fact_params)
      flash[:success] = "Fact updated."
      redirect_to facts_url
    else
      render 'edit'
    end
  end
  
  def index
    @facts = nil
    case
      when params[:active_flag].present?
        @facts = Fact.active(eval(params[:active_flag]))
      when params[:age].present?
        @facts = Fact.age(eval(params[:age]))
      when params[:category].present?
        @facts = Fact.category(params[:category])
      else
        @facts = Fact.all
    end
    
    # stats
    @total_facts = Fact.count
    @active_facts = Fact.active(true).count
    @inactive_facts = Fact.active(false).count
    
    @age_counts = Array.new
    (Child::MIN_CHILD_AGE..Child::MAX_CHILD_AGE).each do |age|
      @age_counts.push(Fact.age(age).count)
    end
    
    @category_counts = Hash.new
    Fact::CATEGORIES.each do |category|
      @category_counts[category] = Fact.category(category).count
    end
    
  end
  
  def destroy
    Fact.find(params[:id]).destroy
    flash[:success] = "Fact deleted."
    redirect_to facts_url
  end
  
  private
    
    def fact_params
      params.require(:fact).permit(:content, :category, :age_low, :age_high, :reference_id, :active_flag)
    end
    
end
