module SessionsHelper
  
  #logs in a user
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # TODO:  store current user in a variable to prevent multiple database hits
  def current_user
    User.find_by(id: session[:user_id])
  end
  
  def current_user?(user)
    user == current_user
  end
  
  def logged_in?
    !current_user.nil?
  end
  
  def admin_user?
    current_user.admin?
  end
  
  def logout(user)
    if (logged_in?)
      session.delete(:user_id)
    end
  end
  
end
