class Fact < ActiveRecord::Base
  
  belongs_to :reference
  
  # Constants
  CATEGORY_NATURE = "nature"
  CATEGORY_PHYSICAL_WORLD = "physical_world"
  CATEGORY_SPACE = "space"
  CATEGORY_VEHICLES = "vehicles"
  CATEGORY_BODY = "body"
  CATEGORY_DINOSAURS = "dinosaurs"
  CATEGORIES = [CATEGORY_NATURE, CATEGORY_PHYSICAL_WORLD, CATEGORY_SPACE, CATEGORY_VEHICLES, CATEGORY_BODY, CATEGORY_DINOSAURS]
  
  validates :content, presence: true, uniqueness: true
  validates :category, inclusion: { in: CATEGORIES}
  validates :age_low, presence: true, numericality: { only_integer: true }
  validates :age_high, presence: true, numericality: { only_integer: true }
  validates :age_low, numericality: {less_than_or_equal_to: :age_high}, unless: "age_high.nil? || age_low.nil?"
  
  # named scopes
  scope :active, lambda { |active| where(active_flag: active) }
  scope :category, lambda { |category| where(category: category) }
  scope :age, lambda { |age| where(":child_age BETWEEN age_low AND age_high", child_age: age) }
  
  def self.select_fact(child)
    history = "SELECT fact_id FROM histories where child_id = :child_id"
    age = ":child_age BETWEEN age_low AND age_high"
    active_flag = "active_flag = :active"
    Fact.where("id NOT IN (#{history}) AND #{active_flag} AND #{age}", child_id: child.id, child_age: child.age, active: true).order("RANDOM()").limit(1).first
  end
  
end
