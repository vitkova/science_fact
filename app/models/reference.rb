class Reference < ActiveRecord::Base
  has_many :fact
  
  validates :citation, presence: true, uniqueness: true
  
end
