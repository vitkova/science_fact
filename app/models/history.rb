class History < ActiveRecord::Base
  belongs_to :fact
  belongs_to :child
  
  # fact should be unique per child
  validates_uniqueness_of :fact_id, :scope => :child_id
  
  scope :child_history, lambda { |child| where(child: child) }
  
end
