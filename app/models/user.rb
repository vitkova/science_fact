class User < ActiveRecord::Base
  
  has_many :children
  
  attr_accessor :reset_token
  
  # save email in lower case
  before_save { self.email = email.downcase }
  
  # first and last name validations
  validates :fname, presence: true, length: {maximum:25}
  validates :lname, presence: true, length: {maximum:25}
  
  # email validations
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum:255},
                        format: { with: VALID_EMAIL_REGEX },
                        uniqueness: {case_sensitive: false}
  
  has_secure_password
  
  # validate password for min 6 characters and at least 1 number
  VALID_PASSWORD_REGEX = /.*[0-9]+.*/i
  validates :password, length: { minimum: 6 },
                 format: { with: VALID_PASSWORD_REGEX, message: " must contain at least 1 number" }, allow_blank: true
                 
  scope :admin, lambda { |admin| where(admin: admin) }

  # creates a digest, used to store a password digest in db
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                              BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  
  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end
  
  # Sends password reset email.
  def send_password_reset_email
    PasswordResetMailer.password_reset(self).deliver_now
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(token)
    digest = reset_digest
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
end
