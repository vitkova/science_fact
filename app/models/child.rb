require 'adroit-age'

class Child < ActiveRecord::Base
  belongs_to :user
  
  before_validation :create_unsubscribe_token, :on => :create

  attr_reader :age
  attr_reader :days_to_next_fact
  
  # Constants
  MIN_CHILD_AGE = 2
  MAX_CHILD_AGE = 7
  NUM_DAYS_BETWEEN_FACTS = 7
  EMAIL_BATCH_SIZE = 500
  
  validates :name, presence: true
  validates :dob, presence: true 
  validates :user, presence: true
  
  # name should be unique per user
  validates_uniqueness_of :name, :scope => :user_id 
  
  # child must be between 1 and 7 years old
  validates_date :dob, before: MIN_CHILD_AGE.year.ago, before_message: "must be at least #{MIN_CHILD_AGE} year old"
  validates_date :dob, after: MAX_CHILD_AGE.years.ago, after_message: "must be less than #{MAX_CHILD_AGE} years old"
  validates :unsubscribe_token, presence: true
  
  def age
    dob.to_date.find_age
  end
  
  def days_to_next_fact
    days = ((last_fact_date + NUM_DAYS_BETWEEN_FACTS) - Date.today).to_i
    if days <= 0
      1
    else
      days
    end
    
  end
  
  def send_fact_email
    fact = Fact.select_fact(self)
    FactMailer.fact_email(self.user, self, fact).deliver_now
    update_attributes(last_fact_date: Date.today)
    History.create!(child:self, fact:fact)
  end

  def self.send_scheduled_emails
   Child.where("last_fact_date <= :date AND subscribed = :subscribed", date: Date.today - NUM_DAYS_BETWEEN_FACTS, subscribed: true).find_each(batch_size: EMAIL_BATCH_SIZE) do |child|
      child.send_fact_email
    end
  end
  
  def unsubscribe
    update_attribute(:subscribed, false)
  end
  
  def subscribe
    update_attribute(:subscribed, true)
    update_attribute(:unsubscribe_token, guid)
  end
  
  private
  
    def create_unsubscribe_token
      self.unsubscribe_token = guid
    end
    
    def guid
      SecureRandom.uuid
    end
  
end
