Rails.application.routes.draw do
 

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'histories/index'

  get 'references/new'
  get 'references/edit'
  get 'references/show'

  get 'stats/show'

  get 'facts/new'

  get 'children/new'

  get 'users/new'
  get 'users/create'
  get 'users/show'
  
  get 'signup'  => 'users#new'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy' 
  
  get   'how_it_works' => 'static_pages#how_it_works'
  get   'about' => 'static_pages#about'

  resources :users
  resources :children
  resources :facts
  resources :references, only: [:new, :create, :edit, :update, :index]
  resources :histories, only: [:index]
  resources :subscriptions, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]

  root 'static_pages#home'

end
