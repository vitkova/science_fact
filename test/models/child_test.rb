require 'test_helper'

class ChildTest < ActiveSupport::TestCase

  def setup
    @child = children(:jenny)
    @unsubscribed_child = children(:jerry)
    @parent = users(:mary)
    @another_parent = users(:bob)
    
    ActionMailer::Base.deliveries.clear
  end
  
  test "new child should be valid" do
    new_child = @parent.children.build(name: "Melissa", dob: Date.today - 5.years)
    assert new_child.valid?
  end

  test "name should not be empty" do
    @child.name = ""
    assert_not @child.valid?
  end
  
  test "dob should not be empty" do
    @child.dob = nil
    assert_not @child.valid?
  end
  
  test "child should not be younger than 1" do
    @child.dob = Date.today - 6.months
    assert_not @child.valid?
  end
 
  test "child should not be older than 7" do
    @child.dob = Date.today - 8.years
    assert_not @child.valid?
  end
  
  test "child must be associated with a user" do
    @child.user = nil
    assert_not @child.valid?
  end
  
  test "child name should be unique per user" do
    @first_child = @parent.children.build(name: "James", dob: Date.today - 5.years)
    assert @first_child.save
    
    @second_child = @parent.children.build(name: "James", dob: Date.today - 3.years)
    assert_not @second_child.save
  end
  
  test "different users can have children with the same name" do
    @first_child = @parent.children.build(name: "James", dob: Date.today - 5.years)
    assert @first_child.save
    
    @second_child = @another_parent.children.build(name: "James", dob: Date.today - 3.years)
    assert @second_child.save
  end
  
  test "child should be subscribed by default" do
    new_child = Child.create(name: "New Child", dob: Date.today - 4.years)
    assert new_child.subscribed?
  end
  
  test "unsubscribe_token should be auto-populated" do
    child = @parent.children.build(name: "Henry", dob: Date.today - 3.years)
    child.save
    assert_not child.unsubscribe_token.nil?
  end
  
  test "unsubscribe_token should be updated upon subscription" do
    old_token = @unsubscribed_child.unsubscribe_token
    
    assert_not_nil old_token
    
    @unsubscribed_child.subscribe
    
    assert @unsubscribed_child.subscribed
    assert_not_equal old_token, @unsubscribed_child.unsubscribe_token
  end
  
  test "unsubscribe child" do
    @child.unsubscribe
    assert_not @child.subscribed
    
  end
  
  test "child with last fact date larger than days between facts" do
    @child.last_fact_date = Date.today - (Child::NUM_DAYS_BETWEEN_FACTS*3)
    @child.save
    
    assert_equal 1, @child.days_to_next_fact
  end
  
  test "child with last fact date equal to days between facts" do
    @child.last_fact_date = Date.today - Child::NUM_DAYS_BETWEEN_FACTS
    @child.save
    
    assert_equal 1, @child.days_to_next_fact
  end
  
  test "child with last fact date less than days between facts" do
    @child.last_fact_date = Date.today - (Child::NUM_DAYS_BETWEEN_FACTS - 4)
    @child.save
    
    assert_equal 4, @child.days_to_next_fact
    
  end
 
end
