require 'test_helper'

class HistoryTest < ActiveSupport::TestCase

  def setup 
    @child1 = children(:jenny)
    @child2 = children(:billy)
    @fact = facts(:spider_monkeys)
  end

  test "can't have duplicate facts per child" do
    h1 = History.new
    h1.child = @child1
    h1.fact = @fact
    h1.save
    
    h2 = History.new
    h2.child = @child1
    h2.fact = @fact
    
    assert_not h2.valid?
    
  end
  
  test "duplicate facts for different chldren is ok" do
    h1 = History.new
    h1.child = @child1
    h1.fact = @fact
    h1.save
    
    h2 = History.new
    h2.child = @child2
    h2.fact = @fact
    
    assert h2.valid?
    
  end

end
