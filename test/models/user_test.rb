require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(fname: "Example", lname: "User", email: "aaaaa@a.com", password: "foobar7", password_confirmation: "foobar7")
  end

  # check that mock user is valid
  test "mock user should be valid" do
    assert @user.valid?
  end

  # first name tests
  test "first name should be present" do
    @user.fname = "     "
    assert_not @user.valid?
  end
  
  test "first name should be less than 25 characters" do
    @user.fname = "a"*26
    assert_not @user.valid?
  end
  
  test "first name can be 25 characters" do
    @user.fname = "a"*25
    assert @user.valid?
  end
  
  test "first name can be less than 25 characters" do
    @user.fname = "a"*5
    assert @user.valid?
  end
  
  # last name tests
  test "last name should be present" do
    @user.lname = "     "
    assert_not @user.valid?
  end
  
    test "last name should be less than 25 characters" do
    @user.lname = "a"*26
    assert_not @user.valid?
  end
  
  test "last name can be 25 characters" do
    @user.lname = "a"*25
    assert @user.valid?
  end
  
  test "last name can be less than 25 characters" do
    @user.lname = "a"*5
    assert @user.valid?
  end
  
  # email validations
   test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
    test "email should be less than 255 characters" do
    @user.email = "a"*256 + "@a.com"
    assert_not @user.valid?
  end
  
  test "email can be 255 characters" do
    @user.email = "a"*249 + "@a.com"
    assert @user.valid?
  end
  
  test "email can be less than 255 characters" do
    @user.email = "a"*5 + "@a.com"
    assert @user.valid?
  end
  
  test "valid email address" do
    @user.email = "john.smith@gmail.com"
    assert @user.valid?
  end
  
  test "email without @" do
    @user.email = "notanemail.com"
    assert_not @user.valid?
  end
  
  test "email without ." do 
    @user.email = "notanemail@examplecom"
    assert_not @user.valid?
  end
  
  test "email with space" do
    @user.email = "not an email"
    assert_not @user.valid?
  end
  
  test "email with two dots" do
    @user.email = "user@example..com"
    assert_not @user.valid?
  end
  
#  test "email should be unique" do
#    dup_email = @user.email
#    @dup_user = User.new(fname: "First", lname: "Last", email: dup_email, password: "password7", password_confirmation: "password7")
#    assert_not @dup_user.valid?
#  end
  
  # password validations
  
  test "password confirmation is blank" do
    @user.password_confirmation = "  "
    assert_not @user.valid?
  end
 
  test "password is less than 6 characters" do
    @user.password = "a2"*2
    @user.password_confirmation = @user.password
    assert_not @user.valid?
  end
  
  test "password is 6 characters" do
    @user.password = "abcde5"
    @user.password_confirmation = @user.password
    assert @user.valid?
  end
  
  test "password is more than 6 characters" do
    @user.password = "a"*6 + "1"
    @user.password_confirmation = @user.password
    assert @user.valid?
  end
  
  test "password without a number" do
    @user.password = "password"
    @user.password_confirmation = @user.password
    assert_not @user.valid?
    assert @user.errors["password"].include?(" must contain at least 1 number")
  end 
  
  test "password with more than 1 number" do 
    @user.password = "4abc5abc8"
    @user.password_confirmation = @user.password
    assert @user.valid?
  end
  
  test "password and password confirmation do not match" do
    @user.password = "foo"
    @user.password_confirmation = "bar"
    assert_not @user.valid?
  end
  
  test "users should be non-admins by default" do
    @user = User.new
    assert_not @user.admin?
  end
  
  test "get only non-admins" do
    @users = User.admin(false)
    
    @users.each do |user|
      assert_not user.admin?
    end
  end
  
  test "get only admins" do
    @users = User.admin(true)
    
    @users.each do |user|
      assert user.admin?
    end
  end
  
end
