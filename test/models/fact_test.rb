require 'test_helper'

class FactTest < ActiveSupport::TestCase

  def setup
    @child = children(:jenny)
  end

  test "content can't be null" do
    @fact = Fact.new
    @fact.content = nil
    @fact.category = Fact::CATEGORY_VEHICLES
    @fact.age_low = 2
    @fact.age_high = 5
    assert_not @fact.valid?
  end
  
  test "content can't be empty" do
    @fact = Fact.new
    @fact.content = ""
    @fact.category = Fact::CATEGORY_VEHICLES
    @fact.age_low = 2
    @fact.age_high = 5
    assert_not @fact.valid?
  end
  
  test "content must be unique" do
    content = "test content"
    Fact.create!(content: content, category: Fact::CATEGORY_VEHICLES, age_low:2, age_high:5, active_flag: true)
    
    fact2 = Fact.new
    fact2.content = content
    fact2.category = Fact::CATEGORY_NATURE
    fact2.active_flag = true
    
    assert_not fact2.valid?
    
  end
  
  test "category must be valid" do
    content = "test content"
    category = "foo bar"
    
    fact = Fact.new(content: content, category: category, age_low: 2, age_high: 5)
    
    assert_not fact.valid? 
  end

  test "valid category" do
    content = "test content"
    category = Fact::CATEGORY_VEHICLES
    
    fact = Fact.new(content: content, category: category, age_low: 2, age_high: 5, active_flag: true)
  
    assert fact.valid? 
  end
  
  test "age_low cannot be null" do
    fact = Fact.new
    fact.content = "sample content"
    fact.category = Fact::CATEGORY_VEHICLES
    fact.age_high = 5
    assert_not fact.valid?
  end
  
  test "age_high cannot be null" do
    fact = Fact.new
    fact.content = "sample content"
    fact.category = Fact::CATEGORY_VEHICLES
    fact.age_low = 2
    assert_not fact.valid?
  end
  
  test "age_low must be an integer" do
    fact = Fact.new
    fact.content = "sample content"
    fact.category = Fact::CATEGORY_VEHICLES
    fact.age_low = "t"
    fact.age_high = 5
    assert_not fact.valid?
    
    fact.age_low = 1.3
    assert_not fact.valid?
  end
  
  test "age_high must be an integer" do
    fact = Fact.new
    fact.content = "sample content"
    fact.category = Fact::CATEGORY_VEHICLES
    fact.age_low =  2
    fact.age_high = "t"
    assert_not fact.valid?
    
    fact.age_high = 1.3
    assert_not fact.valid?
  end
  
  test "age_high can't be smaller than age_low" do
    fact = Fact.new
    fact.content = "sample content"
    fact.category = Fact::CATEGORY_VEHICLES
    fact.age_low = 5
    fact.age_high = 2
    assert_not fact.valid?
  end
  
    test "selecting a fact" do
    fact1 = Fact.select_fact(@child)
    fact2 = Fact.select_fact(@child)
    
    assert_not_nil fact1
    assert_not_nil fact2
    
    assert_not_equal fact1.content, fact2.content
    
  end
  
  test "selected fact cannot be a fact that's in the history" do
    history = History.where(child: @child)
    (1..1000).each do
      fact = Fact.select_fact(@child)
      assert_not fact_in_history?(history, fact)
    end
  end
  
  test "selected fact has to be in child's age range" do
    (1..1000).each do
      fact = Fact.select_fact(@child)
      assert_includes fact.age_low..fact.age_high, @child.age
    end
  end
  
  test "selected fact cannot be inactive" do
    (1..1000).each do
      fact = Fact.select_fact(@child)
      assert_equal true, fact.active_flag
    end
  end
  
  
  test "active_flag is false by default" do
    fact = Fact.new
    fact.content = "Sample content."
    fact.category = Fact::CATEGORY_VEHICLES
    fact.age_low = 2
    fact.age_high = 5
    
    assert fact.valid?
    assert_equal fact.active_flag, false
  end
  
  test "fact count active/inactive" do
    active = Fact.active(true).count
    inactive = Fact.active(false).count
   
    assert_equal Fact.count, active+inactive
  end
  
  test "fact count by age" do
    age_counts = Array.new
    (2..7).each do |age|
      age_counts.push(Fact.age(age).count)
    end
    assert Fact.count < age_counts.reduce(:+)
  end
  
  test "fact count by category" do
    sum = 0
    Fact::CATEGORIES.each_index do |index|
      count = Fact.category(Fact::CATEGORIES[index]).count
      assert count > 0
      sum += count
    end
    assert_equal Fact.count, sum
  end
  
  def fact_in_history?(history, fact)
    result = false
    history.each do |record| 
      result = true if record.fact == fact
    end
    result
  end

end
