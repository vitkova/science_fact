require 'test_helper'

class ReferenceTest < ActiveSupport::TestCase

  def setup
    @fact1 = facts(:komoto_dragons)
    @fact2 = facts(:spider_monkeys)
  end

  test "citation is required" do
    ref = Reference.new
    ref.citation = nil
    assert_not ref.valid?
    
    ref.citation = ""
    assert_not ref.valid?
  end
  
  test "one reference can be associated with more than one fact" do
    ref = Reference.new
    ref.citation = "Sample citation"
    ref.save!
    
    @fact1.reference = ref
    @fact2.reference = ref
    
    assert ref.valid?
    assert @fact1.valid?
    assert @fact2.valid?
    
  end
  
  test "Citation must be unique" do
    citation = "A test citation."
    @ref1 = Reference.new
    @ref1.citation = citation
    @ref1.save
    
    @ref2 = Reference.new
    @ref2.citation = citation
    
    assert_not @ref2.valid?
    
    
  end

end
