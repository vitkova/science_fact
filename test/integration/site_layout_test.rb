require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @admin_user = users(:mary)
    @user = users(:bob)
  end
  
  test "banner links when not logged in" do
    get root_url
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 3
    assert_select "a[href=?]", how_it_works_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", login_path
 end
  
  test "banner links when logged in as admin" do
    log_in_as(@admin_user)
    get root_url
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", facts_path
    assert_select "a[href=?]", references_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", users_path(admin: true)
    assert_select "a[href=?]", logout_path
  end

  test "banner links when logged in as non-admin" do
    log_in_as(@user)
    get root_url
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", logout_path
  end

end
