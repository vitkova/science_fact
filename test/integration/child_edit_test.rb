require 'test_helper'

class ChildEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:mary)
    @child = children(:jenny)
  end
  
  test "editing a child" do
    log_in_as(@user)
    get edit_child_path(@child.id)
    assert_template 'children/edit'
    new_name = "Jenny2"
    patch child_path(@child), child: {name: new_name}
    assert_redirected_to @user
    @updated_child = Child.find(@child.id)
    assert_equal(@updated_child.name, new_name)
  end
  
end
