require 'test_helper'

class FactEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = users(:mary)
    @fact = facts(:spider_monkeys)
  end
  
  test "edit fact" do
    log_in_as(@admin)
    get edit_fact_path(@fact.id)
    assert_template 'facts/edit'
    @fact.content = "Updated content"
    patch fact_path(@fact), fact: {content: @fact.content}
    assert_redirected_to facts_url
    updated_fact = Fact.find(@fact.id)
    assert_equal(@fact.content, updated_fact.content)
  end
  
end
