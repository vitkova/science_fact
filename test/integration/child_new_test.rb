require 'test_helper'

class ChildNewTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:mary)
  end

  test "create new child" do
    log_in_as(@user)
    get new_child_path
    assert_template 'children/new'
    
    # save new child
    #child = Child.new(name: "Jane", dob: DateTime.now - 5.years)
    post children_path child: {name: "Jane", dob: DateTime.now - 5.years, user:@user}
    
    # make sure child was saved to db
    saved_child = Child.find_by(name: "Jane", user: @user)
    assert_not_nil(saved_child)
    
    # make sure email was sent
    assert_equal 1, ActionMailer::Base.deliveries.size
    
    # make sure next fact date was set
    assert_equal Date.today, saved_child.last_fact_date
    
  end

end
