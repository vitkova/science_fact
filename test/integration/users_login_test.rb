require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = User.new(fname: "Example", lname: "User", email: "aaaaa@a.com", password: "foobar7", password_confirmation: "foobar7")
    @admin = users(:mary)
    @non_admin = users(:bob)
  end
  
  test "Empty login credentials" do
    # Visit the login path.
    get login_path
    
    # Verify that the new sessions form renders properly.
    assert_template "sessions/new"
    
    # Post to the sessions path with an invalid params hash.
    post login_path, session: {email: "", password: ""}
    
    # Verify that the new sessions form gets re-rendered and that a flash message appears.
    assert_template "sessions/new"
    assert_not flash.empty?
    
    # Visit another page (such as the Home page).
    get root_path
    
    # Verify that the flash message doesn’t appear on the new page.
    assert flash.empty?
    
  end
  
  test "Invalid login test" do
    # Visit the login path.
    get login_path
    
    # Verify that the new sessions form renders properly.
    assert_template "sessions/new"
    
    # Post to the sessions path with an invalid params hash.
    post login_path, session: {email: @user.email, password: @user.password + "123"}
    
    # Verify that the new sessions form gets re-rendered and that a flash message appears.
    assert_template "sessions/new"
    assert_not flash.empty?
    
    # Visit another page (such as the Home page).
    get root_path
    
    # Verify that the flash message doesn’t appear on the new page.
    assert flash.empty?
  end
  
  test "admins should see the stats page upon login" do
    log_in_as(@admin)
    follow_redirect!
    assert_redirected_to stats_show_path
  end
  
  test "non-admins should see the user page upon login" do
    log_in_as(@non_admin)
    follow_redirect!
    assert_redirected_to users_show_path(id:@non_admin.id)
  end
  
end
