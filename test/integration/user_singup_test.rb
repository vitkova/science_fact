require 'test_helper'

class UserSingupTest < ActionDispatch::IntegrationTest

	def setup
		ActionMailer::Base.deliveries.clear
	end

  test "valid signup information" do
    get signup_path
    assert_template 'users/new'
    assert_difference 'User.count',1 do
		post users_path, user: { fname:  "Example",
		                         lname: "User",
														 email: "user@example.com",
														 password:              "password1",
														 password_confirmation: "password1" }
		end
		
		# make sure welcome email was sent
    assert_equal 1, ActionMailer::Base.deliveries.size
		
		assert_redirected_to children_new_path
  end

end
