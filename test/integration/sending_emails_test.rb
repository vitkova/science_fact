require 'test_helper'

class SendingEmailsTest < ActionDispatch::IntegrationTest

  def setup
    @child = children(:jenny)
    @child2 = children(:billy)
    @child3 = children(:jason)
    @unsubscribed_child = children(:jerry)
    
    ActionMailer::Base.deliveries.clear
  end

  test "sending emails" do
    child_last_fact_date = Date.today - (Child::NUM_DAYS_BETWEEN_FACTS + 2)
    child2_last_fact_date = Date.today - (Child::NUM_DAYS_BETWEEN_FACTS - 5)
    unsubscribed_child_last_fact_date = @unsubscribed_child.last_fact_date
    @child.update_attributes(last_fact_date: child_last_fact_date)
    @child2.update_attributes(last_fact_date: child2_last_fact_date)
    @child3.update_attributes(last_fact_date: Date.today - Child::NUM_DAYS_BETWEEN_FACTS)
    
    # get the number of records with last_fact_date of today prior to sending
    num_sent_today_prior_to_run = Child.where(last_fact_date: Date.today).size
    
    # get the number of history records for this child
    num_history_records_prior_to_run = History.where(child:@child).count
    
    Child.send_scheduled_emails
    
    # ensure at least two emails were sent
    assert ActionMailer::Base.deliveries.size >= 2
    
    @child = Child.find(@child.id)
    @child2 = Child.find(@child2.id)
    @child3 = Child.find(@child3.id)
    @unsubscribed_child = Child.find(@unsubscribed_child.id)
    
    # ensure last_fact_date was updated correctly
    assert_equal Date.today, @child.last_fact_date
    assert_equal child2_last_fact_date, @child2.last_fact_date
    assert_equal Date.today, @child3.last_fact_date
    assert_equal unsubscribed_child_last_fact_date, @unsubscribed_child.last_fact_date
    
    num_sent_today_after_run = Child.where(last_fact_date: Date.today).count
    
    assert num_sent_today_after_run - num_sent_today_prior_to_run >= 2
    
    num_history_records_after_run = History.where(child: @child).count
    
    assert_equal num_history_records_prior_to_run + 1, num_history_records_after_run
    
  end

end
