require 'test_helper'

class FactCreationTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:mary)
  end

  test "invalid content" do
    log_in_as(@admin)
    get new_fact_path
    assert_template 'facts/new'
    assert_no_difference 'Fact.count' do
		  post facts_path, fact: { content:  "", category: Fact::CATEGORY_PHYSICAL_WORLD, age_low:2, age_high: 5}
		end
		assert_template 'facts/new'
  end

  test "valid content" do
    log_in_as(@admin)
    get new_fact_path
    assert_template 'facts/new'
    assert_difference 'Fact.count',1 do
		  post facts_path, fact: { content:  "test", category: Fact::CATEGORY_SPACE, age_low: 2, age_high: 5, active_flag: true}
		end
    assert_redirected_to facts_url
  end

  test "invalid category" do 
       log_in_as(@admin)
    get new_fact_path
    assert_template 'facts/new'
    assert_no_difference 'Fact.count' do
		  post facts_path, fact: { content:  "test", category: "foobar", age_low: 2, age_high: 5}
		end
    assert_template 'facts/new'
  end
  
end
