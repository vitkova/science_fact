require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end
  
  test 'Password resets' do
    
    # get the form where the user enters an email
    get new_password_reset_path
    assert_template 'password_resets/new'
    
    # test an invalid email
    post password_resets_path, password_reset: { email: "" }
    assert_not flash.empty?
    assert_template 'password_resets/new'
    
    # test a valid email
    post password_resets_path, password_reset: {email: @user.email}
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_url
    
    # Password reset form
    user = assigns(:user)
    
    # Wrong email
    get edit_password_reset_path(user.reset_token, email: "")
    assert_not flash.empty?
    assert_redirected_to root_url
    
    
    # Right email, wrong token
    get edit_password_reset_path("abc", email: user.email)
    assert_not flash.empty?
    assert_redirected_to root_url
    
    # Expired link
    original_resent_sent_at = @user.reset_sent_at
    @user.reset_sent_at = Date.today - 1.day
    @user.save
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_not flash.empty?
    assert_redirected_to new_password_reset_url
    @user.reset_sent_at = original_resent_sent_at
    @user.save
    
    # Right email, right token
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select "input[name=email][type=hidden][value=?]", user.email
    
    # Invalid password & confirmation
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "foobaz1",
                  password_confirmation: "barquux1" }
    assert_select 'div#error_explanation'
    
    # Blank password
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "",
                  password_confirmation: "foobaz1" }
    assert_not flash.empty?
    assert_template 'password_resets/edit'
    
    # Valid password & confirmation
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "foobar1",
                  password_confirmation: "foobar1" }
    assert_not flash.empty?
    assert_redirected_to user
    
    
  end

end
