ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # login as the passed in user
  def log_in_as(user, options={})
    password = options[:password] || 'password'
    
    if integration_test?
      post login_path, session: {email: user.email, 
                                password: password}
    else
      session[:user_id] = user.id
    end
  end
  
  # creates a digest based on the passed in string
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  private
    
    def integration_test?
      defined?(post_via_redirect)
    end
  
end
