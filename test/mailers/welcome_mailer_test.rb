require 'test_helper'

class WelcomeMailerTest < ActionMailer::TestCase
  
  def setup
    @user1 = users(:bob)
  end
  
  test "welcome_email" do
    mail = WelcomeMailer.welcome_email(@user1)
    assert_equal "Welcome to Curious Tots!", mail.subject
    assert_equal [@user1.email], mail.to
    assert_equal ["noreply@curious-tots.com"], mail.from
  end

end
