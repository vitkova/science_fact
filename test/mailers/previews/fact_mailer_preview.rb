# Preview all emails at http://localhost:3000/rails/mailers/fact_mailer
class FactMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/fact_mailer/fact_email
  def fact_email
    child = Child.first
    user = child.user
    fact = Fact.first
    FactMailer.fact_email(user, child, fact)
  end

end
