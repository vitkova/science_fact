require 'test_helper'

class FactMailerTest < ActionMailer::TestCase
  
  def setup
    @user = users(:bob)
    @child = children(:jason)
    @fact = facts(:spider_monkeys)
  end
  
  test "fact_email" do
    mail = FactMailer.fact_email(@user, @child, @fact)
    assert_equal [@user.email], mail.to
    assert_match @fact.content, mail.body.encoded
    assert_match @child.name, mail.body.encoded
  end

end
