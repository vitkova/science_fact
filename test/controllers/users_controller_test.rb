require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:mary)
    @other_user = users(:bob)
    @user3 = users(:michael)
  end
  
  test "should only show profile if user is logged in" do
    get :show, id: @user
    assert_redirected_to login_url
  end
  
  test "should only show profile of currently logged in user" do
    log_in_as(@user3)
    get :show, id: @other_user
    assert_redirected_to root_url
  end
  
  test "admin can see profile of other users" do
    log_in_as(@user)
    get :show, id: @other_user
    assert_template 'users/show'
  end
  
  test "should only show users if logged in user is admin" do
    # login as non-admin
    log_in_as(@other_user)
    get :index
    assert_redirected_to root_url
    
    #login as admin
    log_in_as(@user)
    get :index
    assert_template 'users/index'
  end

  
  test "should redirect to admins after creating an admin" do
    log_in_as(@user)
    get :new, admin: true
    assert_template 'users/new'
    assert_difference 'User.count',1 do
		post :create, user: { fname:  "Example",
		                         lname: "User",
														 email: "user@example.com",
														 password:              "password1",
														 password_confirmation: "password1",
														 admin: true}
		end
		assert_redirected_to users_path(admin:true)
  end
  
  test "edit admin test" do
    log_in_as(@user)
    get :edit, id: @user.id
    assert_template 'users/edit'
    post :update, id: @user.id, user: {fname: "New", lname: "Name", email: "new_email@example.com"}
    updated_user = User.find(@user.id)
    
    assert_equal "New", updated_user.fname
    assert_equal "Name", updated_user.lname
    assert_equal "new_email@example.com", updated_user.email
  end
  
  test "can't edit or update if logged in as non-admin" do
    log_in_as(@other_user)
    get :edit, id: @user.id
    assert_redirected_to root_url
    
    post :update, id: @user.id, user: {fname: "New", lname: "Name", email: "new_email@example.com"}
    assert_redirected_to root_url
    updated_user = User.find(@user.id)
    
    assert_equal @user.fname, updated_user.fname
    assert_equal @user.lname, updated_user.lname
    assert_equal @user.email, updated_user.email
    
  end
  
end
