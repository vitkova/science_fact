require 'test_helper'

class ChildrenControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:mary)
    @other_user = users(:bob)
    @users_child = children(:jenny)
  end
  
  
  test "should only display if user is logged in" do
    get :new
    assert_redirected_to login_url
    
    log_in_as(@user)
    get :new
    assert_template 'children/new'
    
  end
  
  test "should not be able to edit child if not logged in" do
    get :edit, id:@users_child
    assert_redirected_to login_url
  end
  
  test "should not be able to edit someone else's child" do
    log_in_as(@other_user)
    get :edit, id:@users_child
    assert_redirected_to root_url
  end
  
  test "should be able to edit your own child" do
    log_in_as(@user)
    get :edit, id:@users_child
    assert_template 'children/edit'
  end
  
end
