require 'test_helper'

class StatsControllerTest < ActionController::TestCase

  def setup
    @admin = users(:mary)
    @non_admin = users(:bob)
  end

  test "only admins should see stats page" do
    log_in_as(@non_admin)
    get :show
    assert_redirected_to root_url
    
    log_in_as(@admin)
    get :show
    assert_template "stats/show"
  end

end
