require 'test_helper'

class HistoriesControllerTest < ActionController::TestCase

  def setup
    @admin = users(:mary)
    @user1 = users(:bob)
    @user2 = users(:michael)
  end

  test "can't see history if not parent or admin" do
    child = @user1.children.first
    log_in_as(@user2)
    get :index, child_id: child.id
    assert_redirected_to root_path
  end

  test "can see history of your own children" do
    child = @user1.children.first
    log_in_as(@user1)
     get :index, child_id: child.id
    assert_template 'histories/index'
  end
  
  test "can see history of any children if you are admin" do
    child = @user1.children.first
    log_in_as(@admin)
     get :index, child_id: child.id
    assert_template 'histories/index'
  end

end
