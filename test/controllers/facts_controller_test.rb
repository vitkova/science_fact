require 'test_helper'

class FactsControllerTest < ActionController::TestCase

  def setup
    @admin_user = users(:mary)
    @user = users(:bob)
    @fact = facts(:spider_monkeys)
  end
  
  test "can't access new if not logged in" do
    get :new
    assert_redirected_to login_url
  end
  
  test "can't access new if not admin user" do
    log_in_as(@user)
    get :new
    assert_redirected_to root_url
  end

  test "can access new if admin user" do
    log_in_as(@admin_user)
    get :new
    assert_template "facts/new"
  end
  
  test "can't delete if not logged in" do
    fact = Fact.new(content: "Test content", category: Fact::CATEGORY_NATURE, age_low: 2, age_high: 5)
    fact.save
    assert_no_difference 'Fact.count' do
      delete :destroy, id: fact
    end
  end

  test "can't delete if not admin" do
    log_in_as(@user)
    fact = Fact.new(content: "Test content", category: Fact::CATEGORY_PHYSICAL_WORLD, age_low: 2, age_high: 5)
    fact.save
    assert_no_difference 'Fact.count' do
      delete :destroy, id: fact
    end
  end
  
  test "delete fact" do
    log_in_as(@admin_user)
    fact = Fact.new(content: "Test content", category: Fact::CATEGORY_SPACE, age_low: 2, age_high: 5)
    fact.save
    assert_difference 'Fact.count',-1 do
      delete :destroy, id: fact
    end
  end
  
  test "can't edit if not logged in" do
    @fact.content = "Spider monkeys have SUPER long arms"
    patch :update, id: @fact, fact: {content: @fact.content}
    updated_fact = Fact.find(@fact.id)
    assert_not_equal(@fact.content, updated_fact.content)
  end
  
  test "can't edit if not admin" do
    log_in_as(@user)
    @fact.content = "Spider monkeys have SUPER long arms"
    patch :update, id:@fact, fact: {content: @fact.content}
    updated_fact = Fact.find(@fact.id)
    assert_not_equal(@fact.content, updated_fact.content)
  end
  
  test "edit fact" do
    log_in_as(@admin_user)
    @fact.content = "Spider monkeys have SUPER long arms"
    patch :update, id: @fact, fact: {content: @fact.content}
    updated_fact = Fact.find(@fact.id)
    assert_equal(@fact.content, updated_fact.content)
  end
end