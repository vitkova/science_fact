require 'test_helper'

class ReferencesControllerTest < ActionController::TestCase

  def setup
    @admin = users(:mary)
    @non_admin= users(:bob)
    
    @ref = references(:insideyouroutside)
  end

  test "can't see references if not logged in" do
    get :index
    assert_redirected_to login_url
  end
  
  test "can't see references if not admin" do
    log_in_as(@non_admin)
    get :index
    assert_redirected_to root_url
  end

  test "create reference" do
    log_in_as(@admin)
    get :new
    assert_template 'references/new'
    
    assert_difference 'Reference.count',1 do
      post :create, reference: {citation: "Test citation."}
    end
    
  end

  test "update reference" do
    log_in_as(@admin)
    get :edit, id: @ref.id
    assert_template 'references/edit'
    
    new_citation = "This is a new citation."
    
    patch :update, id: @ref.id, reference: {citation: new_citation}
    
    updated_ref = Reference.find(@ref.id)
    
    assert_equal updated_ref.citation, new_citation
    
  end

end
