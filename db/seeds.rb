# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Admin user
user = User.find_or_initialize_by(email: "admin@curious-tots.com")
user.fname = "Admin"
user.lname = "User"
user.password = "password1"
user.password_confirmation = "password1"
user.admin = true
user.save!
          
# References
r1 = Reference.find_or_initialize_by(citation: "Rabe, Tish, and Aristides Ruiz. Inside Your Outside! New York: Random House, 2003. Print.")
r1.save!

r2 = Reference.find_or_initialize_by(citation:  "Rabe, Tish, Aristides Ruiz, and Joe Mathieu. Miles and Miles of Reptiles. New York: Random House, 2009. Print.")
r2.save!

r3 = Reference.find_or_initialize_by(citation:  "Hughes, Catherine D. Little Kids Big Book of Animals. Washington, D.C.: National Geographic, 2010. Print.")
r3.save!

r4 = Reference.find_or_initialize_by(citation: "Hughes, Catherine D., and Franco Tempesta. Little Kids First Big Book of Dinosaurs. Washington, D.C.: National Geographic, 2011. Print.")
r4.save!

r5 = Reference.find_or_initialize_by(citation: "Hughes, Catherine D., and David A. Aguilar. Little Kids First Big Book of Space. Washington, D.C: National Geographic Society, 1220. Print.")
r5.save!

r6 = Reference.find_or_initialize_by(citation: "Becklake, Sue. 100 Facts: Flight. Essex, UK: Miles Kelly, 2010. Print.")
r6.save!

r7 = Reference.find_or_initialize_by(citation: "\"Ballooning (spider).\" Wikipedia. Wikimedia Foundation, n.d. Web. 14 Apr. 2015.")
r7.save!

#r2 = Reference.find_or_initialize_by(citation:  "")
#r2.save!


# Facts          
   
fact = Fact.find_or_initialize_by(content:"Komodo dragons are the biggest lizards. They can grow to be 10 ft long and 150 pounds. That's as big as a grownup human!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 7
fact.reference = r2
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Animals with wide feet can walk on the snow without sinking.")
fact.category = Fact::CATEGORY_PHYSICAL_WORLD
fact.age_low = 5
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"When it's summer, snowshoe hares turn brown and when it's winter they turn white. This helps them camouflage.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 3
fact.age_high = 5
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Some sharks lay eggs in their tummy and some sharks lay eggs outside their tummy.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 4
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Geckos can climb on walls and windows. They can even walk upside-down on the ceiling!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 3
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Geckos wash their eyes by licking them with their tongue!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 3
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Gravity pulls you back down to the earth when you jump. On the moon the gravity is weaker, so you can jump higher than you can on earth. In space there is no gravity so you can float!")
fact.category = Fact::CATEGORY_SPACE
fact.age_low = 4
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Some plants eat bugs! The venus fly trap has leaves shaped like traps. When a bug lands on the traps, the traps close and eat the bug.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 3
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"The rattle snake has a tail that makes a rattling sound. It uses its tail to warn predators to stay away.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 4
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"When it's daytime in America it's nighttime in China.")
fact.category = Fact::CATEGORY_PHYSICAL_WORLD
fact.age_low = 3
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Cows make the milk we drink.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"You can mix two colors to make a new color. Yellow and blue makes green. Red and blue makes purple.")
fact.category = Fact::CATEGORY_PHYSICAL_WORLD
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"In the spring birds make nests and lay eggs in the nests. Baby birds hatch from the eggs!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Bees collect pollen from flowers and use it to make honey!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Spiders make webs to trap bugs to eat.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Rain and snow come from clouds.")
fact.category = Fact::CATEGORY_PHYSICAL_WORLD
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"In the fall the leaves on trees turn pretty colors like yellow, red and orange. In the winter the leaves fall off the trees.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"The earth is always spinning around itself. When your part of the earth is facing the sun, it's daytime. When your part of the earth is not facing the sun, it's nighttime.")
fact.category = Fact::CATEGORY_SPACE
fact.age_low = 4
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"The sun is a giant ball of fire. It gives us warmth and light.")
fact.category = Fact::CATEGORY_SPACE
fact.age_low = 3
fact.age_high = 5
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Two astronauts called Neil Armstrong and Buzz Aldrin used a spaceship to land on the moon. They walked on the moon and collected space rocks so that scientists could study them.")
fact.category = Fact::CATEGORY_SPACE
fact.age_low = 4
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"The international space station is a space laboratory that orbits around the earth. People who work at the international space station float since there is no gravity.")
fact.category = Fact::CATEGORY_SPACE
fact.age_low = 4
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Cement trucks mix the cement while they drive so that the cement doesn't become hard.")
fact.category = Fact::CATEGORY_VEHICLES
fact.age_low = 2
fact.age_high = 5
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"The largest plane in the world is the Antonov An-225. It can carry the Russian space shuttle on its back!")
fact.category = Fact::CATEGORY_VEHICLES
fact.age_low = 3
fact.age_high = 7
fact.reference = r6
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"When germs make you sick the white blood cells in your body find the germs and eat them!")
fact.category = Fact::CATEGORY_BODY
fact.age_low = 3
fact.age_high = 7
fact.reference = r1
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"When dust gets in your nose your nose sneezes to get the dust out.")
fact.category = Fact::CATEGORY_BODY
fact.age_low = 2
fact.age_high = 5
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Muscles make your body move.")
fact.category = Fact::CATEGORY_BODY
fact.age_low = 2
fact.age_high = 5
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"An octopus has 8 legs!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Your brain is the size of your two fists put together.")
fact.category = Fact::CATEGORY_BODY
fact.age_low = 4
fact.age_high = 7
fact.reference = r1
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Water can be in one of three states: solid, liquid or gas. If you put water in the freezer it will turn into ice. This is the solid state. If you heat water on the stove it will turn into steam. That's the gas phase.")
fact.category = Fact::CATEGORY_PHYSICAL_WORLD
fact.age_low = 5
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Some animals sleep during the day and are active at night. These animals are called nocturnal animals. Bats and flying squirrels are nocturnal.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 5
fact.age_high = 7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Plants need sunshine and water to grow.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

# CITE ANIMAL BOOK HERE
fact = Fact.find_or_initialize_by(content:"Worms live underground in the dirt. They come out when it rains.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"If you could drive to the sun in a car it would take more than 170 years to get there.")
fact.category = Fact::CATEGORY_SPACE
fact.age_low = 5
fact.age_high = 7
fact.reference = r5
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Tyranosaurus Rex had teeth as big as bananas!")
fact.category = Fact::CATEGORY_DINOSAURS
fact.age_low = 2
fact.age_high = 5
fact.reference = r4
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Cheetas are the fastest land animal. They can run at 65 mph. That's as fast as a car on the highway!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 4
fact.age_high = 7
fact.reference = r3
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Baby frogs are called tadpoles. They don't have legs and they have a long tail. They live in the water. As tadpoles grow older their back legs appear. Then their front legs grow in. Finally their tail shrinks and they become frogs.")
fact.category = Fact::CATEGORY_NATURE 
fact.age_low = 3
fact.age_high = 5
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"After hatching, some baby spiders climb a tall tree and use their spider silk to make a parachute. They then use the parachute to float through the air to a place far away from where they were born.")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 3
fact.age_high = 7
fact.reference = r7
fact.active_flag = true
fact.save!

fact = Fact.find_or_initialize_by(content:"Snakes smell with their tongue!")
fact.category = Fact::CATEGORY_NATURE
fact.age_low = 2
fact.age_high = 7
fact.reference = r2
fact.active_flag = true
fact.save!


