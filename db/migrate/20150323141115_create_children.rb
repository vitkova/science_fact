class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.string :name
      t.date :dob
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :children, :users
  end
end
