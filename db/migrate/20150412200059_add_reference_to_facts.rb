class AddReferenceToFacts < ActiveRecord::Migration
  def change
    add_reference :facts, :reference, index: true
    add_foreign_key :facts, :references
  end
end
