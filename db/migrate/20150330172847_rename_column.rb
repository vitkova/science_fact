class RenameColumn < ActiveRecord::Migration
  def change
    rename_column :children, :next_fact_date, :last_fact_date
  end
end
