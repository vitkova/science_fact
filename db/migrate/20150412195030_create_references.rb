class CreateReferences < ActiveRecord::Migration
  def change
    create_table :references do |t|
      t.string :citation

      t.timestamps null: false
    end
  end
end
