class AddAgeHighToFacts < ActiveRecord::Migration
  def change
    add_column :facts, :age_high, :integer
  end
end
