class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.references :fact, index: true
      t.references :child, index: true

      t.timestamps null: false
    end
    add_foreign_key :histories, :facts
    add_foreign_key :histories, :children
    add_index :histories, [:fact_id, :child_id], :unique => true
  end
end
