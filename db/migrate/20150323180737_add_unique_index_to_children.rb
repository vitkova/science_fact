class AddUniqueIndexToChildren < ActiveRecord::Migration
  def change
    add_index :children, [:name, :user_id], :unique => true
  end
end
