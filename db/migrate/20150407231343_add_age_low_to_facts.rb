class AddAgeLowToFacts < ActiveRecord::Migration
  def change
    add_column :facts, :age_low, :integer
  end
end
