class AddCategoryToFacts < ActiveRecord::Migration
  def change
    add_column :facts, :category, :string
  end
end
