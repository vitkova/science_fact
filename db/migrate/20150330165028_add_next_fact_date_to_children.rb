class AddNextFactDateToChildren < ActiveRecord::Migration
  def change
    add_column :children, :next_fact_date, :date
  end
end
