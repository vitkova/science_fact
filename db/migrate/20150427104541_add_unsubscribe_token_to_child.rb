class AddUnsubscribeTokenToChild < ActiveRecord::Migration
  def change
    add_column :children, :unsubscribe_token, :string
  end
end
