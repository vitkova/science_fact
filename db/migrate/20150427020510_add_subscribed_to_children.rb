class AddSubscribedToChildren < ActiveRecord::Migration
  def change
    add_column :children, :subscribed, :boolean, default: true
  end
end
