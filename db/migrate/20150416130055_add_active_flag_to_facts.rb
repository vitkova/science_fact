class AddActiveFlagToFacts < ActiveRecord::Migration
  def change
    add_column :facts, :active_flag, :boolean, null: false, default: false
  end
end
