# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150503225655) do

  create_table "children", force: true do |t|
    t.string   "name"
    t.date     "dob"
    t.integer  "user_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.date     "last_fact_date"
    t.boolean  "subscribed",        default: true
    t.string   "unsubscribe_token"
  end

  add_index "children", ["name", "user_id"], name: "index_children_on_name_and_user_id", unique: true
  add_index "children", ["user_id"], name: "index_children_on_user_id"

  create_table "facts", force: true do |t|
    t.text     "content"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "category"
    t.integer  "age_low"
    t.integer  "age_high"
    t.integer  "reference_id"
    t.boolean  "active_flag",  default: false, null: false
  end

  add_index "facts", ["reference_id"], name: "index_facts_on_reference_id"

  create_table "histories", force: true do |t|
    t.integer  "fact_id"
    t.integer  "child_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "histories", ["child_id"], name: "index_histories_on_child_id"
  add_index "histories", ["fact_id", "child_id"], name: "index_histories_on_fact_id_and_child_id", unique: true
  add_index "histories", ["fact_id"], name: "index_histories_on_fact_id"

  create_table "references", force: true do |t|
    t.string   "citation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: true do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

end
